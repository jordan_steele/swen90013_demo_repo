# Workshop Outline

1. Everyone clone the repository
2. Everyone switch to the develop branch
3. Everyone checkout a new branch titled 'feature/whatever-you-want'
4. Everyone create a new file titled with their name
5. Everyone add the file to tracking
6. Everyone commit their changes
7. Jordan make a pull request back to develop for his branch
8. Demo making code review comments, approving and merging the pull request
9. Everyone rebase their branch onto develop
10. Everyone make a pull request for their branch back into develop

Perhaps repeat but with 2 people and a merge conflict.
